# Instructions for installing SayEye by using Docker
## First part
[Video](https://gitlab.com/opendot/sayeye/sayeye-documentation/-/blob/22fc1c25520720835b855e3ac5354487fc527fbc/01_docker_pre_install.mp4)
1. Download the Docker installer [here](https://www.docker.com/products/docker-desktop)
1. Give permission to install
1. Follow the installer’s steps 
1. At the end, **RESTART**
 
## Second part
[Video](https://gitlab.com/opendot/sayeye/sayeye-documentation/-/blob/22fc1c25520720835b855e3ac5354487fc527fbc/02_docker_post_install.mp4)
1. Docker will start at launch, but will display an error
1. Click on the error message link (do not press "restart" yet)
1. Install the update
1. Click on restart
1. Wait for the Docker status to be "running" (in the bottom left the state color should go from yellow to green - it can take few minutes)
 
After doing these steps you can install **Sayeye**. Remember that - before Docker - you have to install the Tobii bar with its software; otherwise the Sayeye installer will not let you continue.
